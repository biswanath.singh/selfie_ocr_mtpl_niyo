import 'dart:async';

import 'package:flutter/services.dart';

class FlutterTestSelfiecapture {
  static const MethodChannel _channel = const MethodChannel('selfie_ocr_mtpl');

  static Future<String?> get platformVersion async {
    try {
      final String? version = await _channel.invokeMethod('getPlatformVersion');
      return version;
    } catch (e) {
      rethrow;
    }
  }

  static Future<String?> detectLiveliness(
      String? msgselfieCapture, String? msgBlinkEye) async {
    try {
      final String? filePath = await _channel.invokeMethod('detectLiveliness',
          {"msgselfieCapture": msgselfieCapture, "msgBlinkEye": msgBlinkEye});
      return filePath;
    } catch (e) {
      rethrow;
    }
  }

  static Future<dynamic> ocrFromDocumentImage(
      {String? imagePath,
      String? destFaceImagePath,
      int? xOffset,
      int? yOffset}) async {
    try {
      final ocrLines = await _channel.invokeMethod('ocrFromDocImage', {
        "imagePath": imagePath,
        "destFaceImagePath": destFaceImagePath,
        "xOffset": xOffset,
        "yOffset": yOffset,
      });
      return ocrLines;
    } catch (e) {
      rethrow;
    }
  }
}
