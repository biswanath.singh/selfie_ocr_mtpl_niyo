import Flutter
import UIKit
import AVFoundation

let navBar = UINavigationController.init()
var receivedPath = String()

var resultDismiss : FlutterResult!
var resultScanDismiss : FlutterResult!

public class SwiftFlutterTestSelfiecapturePlugin: NSObject, FlutterPlugin, DismissProtocol {

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "selfie_ocr_mtpl", binaryMessenger: registrar.messenger())
        let instance = SwiftFlutterTestSelfiecapturePlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
     
        if (call.method == "getPlatformVersion") {
            result("iOS " +  UIDevice.current.systemVersion)
        }
        else if (call.method == "ocrFromDocImage"){
            resultScanDismiss = result
            
            var tmpImagePath = ""
            var tmpFaceImagePath = ""
            var tmpXoffSet = 0
            var tmpYoffSet = 0
            guard let args = call.arguments else {
                return
            }
            if let myArgs = args as? [String: Any],
                let tmp_ImagePath = myArgs["imagePath"] as? String,
                let tmp_FaceImagePath = myArgs["destFaceImagePath"] as? String,
                let tmp_XoffSet = myArgs["xOffset"] as? Int,
                let tmp_YoffSet = myArgs["yOffset"] as? Int{

                tmpImagePath = tmp_ImagePath
                tmpFaceImagePath = tmp_FaceImagePath
                tmpXoffSet = tmp_XoffSet
                tmpYoffSet = tmp_YoffSet
            }

        }
        else if call.method == "detectLiveliness" {
            resultDismiss = result
            
            var msgselfieCapture = ""
            var msgBlinkEye = ""
            guard let args = call.arguments else {
                return
            }
            if let myArgs = args as? [String: Any],
                let captureText = myArgs["msgselfieCapture"] as? String,
                let blinkText = myArgs["msgBlinkEye"] as? String {
                msgselfieCapture = captureText
                msgBlinkEye = blinkText
            }

            switch AVCaptureDevice.authorizationStatus(for: .video) {
                case .denied:
                    print("Denied, request permission from settings")
                    presentCameraSettings()
                case .restricted:
                    print("Restricted, device owner must approve")
                    
                case .authorized:
                    print("Authorized, proceed")
                    self.detectLiveness(captureMessage: msgselfieCapture, blinkMessage: msgBlinkEye)
                    
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for: .video) { success in
                        if success {
                            print("Permission granted, proceed")
                            self.detectLiveness(captureMessage: msgselfieCapture, blinkMessage: msgBlinkEye)     
                        } else {
                            print("Permission denied")
                        }
                    }
            }
            
        } 
        
       
    }
    
    public func detectLiveness(captureMessage: String, blinkMessage: String) {

         DispatchQueue.main.async {
            if let viewController = UIApplication.shared.keyWindow?.rootViewController as? FlutterViewController{
            let storyboardName = "MainLive"
            let storyboardBundle = Bundle.init(for: type(of: self))
            let storyboard = UIStoryboard(name: storyboardName, bundle: storyboardBundle)
            if let vc = storyboard.instantiateViewController(withIdentifier: "TestViewController") as? TestViewController {
                vc.captureMessageText = captureMessage
                vc.modalPresentationStyle = .fullScreen
                vc.blinkMessageText = blinkMessage
                viewController.present(vc, animated: true, completion: nil)
                vc.dismissDelegate = self
            }
        }
         }
        
       
    }
    
    func sendData(filePath: String) {
        receivedPath = filePath
        if resultDismiss != nil{
            resultDismiss(filePath)
        }else{
            resultDismiss("")
        }
    }

   
    func presentCameraSettings() {
        
        if let viewController = UIApplication.shared.keyWindow?.rootViewController as? FlutterViewController{
            let alertController = UIAlertController(title: "Niyo SBM will require camera access to capture photo for Aadhaar validation",
                                    message: "Camera access is denied, please provide access to continue. After permission change, app will restart for the changes to effect.",
                                    preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })

        viewController.present(alertController, animated: true)
        }

        
    }
    
    func cropImage(image:UIImage, toRect rect:CGRect) -> UIImage {
        let imageRef:CGImage = image.cgImage!.cropping(to: rect)!
        let croppedImage:UIImage = UIImage(cgImage:imageRef)
        return croppedImage
    }
    
    func saveImage(image: UIImage, tmp_path: String, items : [String]) -> [String: AnyObject] {
        self.clearTempFolder()
        var resultOfOCR = [String: AnyObject]()
        var path = tmp_path
        let rotatedimage = image//.rotate(radians: .pi/2)
        
        guard let data = rotatedimage.jpegData(compressionQuality: 1) ?? rotatedimage.pngData() else {
            return (resultOfOCR)
        }


        if path == ""{
            guard let directory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
                return (resultOfOCR)
            }
            path = directory
            path = "\(path)/faceImage.jpeg"
        }

        do {
            try data.write(to: URL.init(fileURLWithPath: path))
            print(path)
            resultOfOCR["ExtractedData"] = items as AnyObject
            resultOfOCR["FaceImagePath"] = path as AnyObject
            if resultScanDismiss != nil{
                resultScanDismiss(resultOfOCR)
            }else{
                resultScanDismiss(resultOfOCR)
            }
            return (resultOfOCR)
        } catch {
            print(error.localizedDescription)
            resultScanDismiss(resultOfOCR)

            return (resultOfOCR)
        }
    }

    func clearTempFolder() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: tempFolderPath + filePath)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }

}
